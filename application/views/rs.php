<!DOCTYPE HTML>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1" />
    <title>carmurs</title>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/fonts/css/fontawesome-all.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/styles/framework.css">
    <link
        href="../../../fonts.googleapis.com/css09dd.css?family=Poppins:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i|Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i"
        rel="stylesheet">
</head>

<body>
    <div id="preloader" class="preloader-light">
        <h1 class="center-text color-black ultrabold uppercase bottom-0 fa-2x">carmurs</h1>
        <div id="preload-spinner"></div>
        <p>Cara Mudah Untuk Rumah Sakit</p>
        <em>Sekolah tinggi ilmu kesehatan </em>
    </div>
    <div id="page-transitions" class="page-build">
        <div class="page-bg gradient-body-1"></div>
        <div class="header shadow-large header-light header-logo-app">
            <a href="index.html" class="header-logo"></a>
            <a data-menu="menu-1" data-menu-type="menu-sidebar-left-push" href="#" class="header-icon header-icon-1"><i
                    class="fas fa-bars"></i></a>
            <a data-menu="menu-contact" data-menu-type="menu-box-modal" href="#" class="header-icon header-icon-2"><i
                    class="fas fa-envelope"></i></a>
            <a data-menu="menu-bell" data-menu-type="menu-box-top" href="#" class="header-icon header-icon-3"><i
                    class="fas fa-bell"></i></a>
            <a data-menu="menu-settings" data-menu-type="menu-box-full" href="#" class="header-icon header-icon-4"><i
                    class="fas fa-cog fa-spin font-12"></i></a>
        </div>
        <div id="menu-1" class="menu menu-sidebar-left menu-settings">
            <div class="menu-bg gradient-body-1"></div>
            <div class="menu-scroll">
                <div class="menu-header">
                    <a class="menu-logo" href="index.html"></a>
                    <h1>carmurs</h1>
                    <p>Cara Mudah Untuk Rumah Sakit</p>
                </div>
                <div class="menu-list icon-background-active">
                    <em class="menu-divider top-10">Navigation</em>
                    <a class="menu-item active-menu" href="<?php echo base_url() ?>"><i
                            class="fa gradient-red-light fa-star"></i>Beranda</a>
                    <a class="menu-item" href="<?php echo base_url() ?>rumasakit"><i
                            class="fa gradient-green-light fa-home"></i>Rumah Sakit</a>
                    <a class="menu-item" href="<?php echo base_url() ?>map"><i
                            class="fa gradient-sky-light fa-cog"></i>lokasi<span>HOT</span></a>
                    <a class="menu-item" href="<?php echo base_url() ?>tiket"><i
                            class="fa gradient-mint-dark fa-hand-point-right"></i>Tiket<span>NEW</span></a>
                    <a data-submenu="submenu-1" class="menu-item" href="#"><i
                            class="fa gradient-magenta-light fa-image"></i>Penilian Rumah Sakit<span></span></a>

                    <a class="menu-item" href="contact.html"><i class="fa gradient-sky-dark fa-envelope"></i>Contact
                        Us</a>
                    <a class="menu-item close-menu" href="#"><i class="fa gradient-red-dark fa-times"></i>Close Menu</a>
                    <em class="menu-divider">Get in Touch</em>
                    <a class="menu-item" href="tel:+1 234 567 890"><i class="fa gradient-green-light fa-phone"></i>Call
                        Us</a>
                    <a class="menu-item" href="sms:+1 234 567 890"><i class="fa gradient-mint-dark fa-comment"></i>SMS
                        Us</a>
                    <a class="menu-item" href="mailto:name@domain.com"><i
                            class="fa gradient-sky-dark fa-envelope"></i>Mail Us</a>
                    <em class="menu-divider">Let's get Social</em>
                    <a class="menu-item" href="https://www.facebook.com/enabled.labs/"><i
                            class="fab facebook-bg fa-facebook-f"></i>Facebook</a>
                    <a class="menu-item" href="https://twitter.com/iEnabled"><i
                            class="fab twitter-bg fa-twitter"></i>Twitter</a>
                    <a class="menu-item" href="https://plus.google.com/u/1/+EnabledLabs"><i
                            class="fab google-bg fa-google-plus-g"></i>Google+</a>
                </div>
                <em class="menu-copyright">Copyright <span class="copyright-year"></span>, Enabled. All Rights
                    Reserved</em>
            </div>
        </div>

        <div id="menu-contact" data-menu-height="460" class="menu menu-box-modal bg-white shadow-large">
            <div class="menu-scroll">
                <h3 class="uppercase ultrabold center-text top-0 color-black font-27">carmurs 2020</h3>
                <p class="center-text left-30 right-30 bottom-20 color-black top-10 opacity-70">
                    Masukan untuk kami.
                </p>
                <div class="deco-thin deco-2 bottom-20"></div>
                <div class="contact-form">
                    <div class="formSuccessMessageWrap" id="formSuccessMessageWrap">
                        <p class="center-text"><i class="fa fa-2x fa-paper-plane-o"></i></p>
                        <h4 class="uppercase ultrabold center-text">Message Sent</h4>
                        <p class="center-text boxed-text">
                            We usually reply in less than 24 hours. <br /> Thank you for getting in touch with us!
                        </p>
                        <a href="#"
                            class="button button-round color-white button-blue button-xs button-center close-menu uppercase bold top-30">Return
                            to Site</a>
                    </div>
                    <form action="https://enableds.com/products/kolor3/php/contact.php" method="post"
                        class="contactForm" id="contactForm">
                        <fieldset>
                            <div class="formValidationError" id="contactNameFieldError">Name is required!</div>
                            <div class="formValidationError" id="contactEmailFieldError">Mail address required!</div>
                            <div class="formValidationError" id="contactEmailFieldError2">Mail address must be valid!
                            </div>
                            <div class="formValidationError" id="contactMessageTextareaError"></div>
                            <div class="formFieldWrap">
                                <label class="field-title contactNameField" for="contactNameField"></label>
                                <input type="text" name="contactNameField" placeholder="John Doe" value=""
                                    class="contactField requiredField" id="contactNameField" />
                            </div>
                            <div class="formFieldWrap">
                                <label class="field-title contactEmailField" for="contactEmailField"></label>
                                <input type="text" name="contactEmailField" placeholder="Email Address" value=""
                                    class="contactField requiredField requiredEmailField" id="contactEmailField" />
                            </div>
                            <div class="clear"></div>
                            <div class="formTextareaWrap half-bottom">
                                <label class="field-title contactMessageTextarea" for="contactMessageTextarea"></label>
                                <textarea name="contactMessageTextarea" placeholder="Enter your message here."
                                    class="contactTextarea requiredField" id="contactMessageTextarea"></textarea>
                            </div>
                            <div class="contactFormButton">
                                <input type="submit"
                                    class="buttonWrap button-blue color-white button-sm uppercase ultrabold shadow-icon-large button-rounded contactSubmitButton"
                                    id="contactSubmitButton" value="Send Message" data-formId="contactForm" />
                            </div>
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>

        <div class="menu menu-box-full shadow-large" id="menu-settings">
            <div class="page-bg gradient-body-1">
                <div class="cover-content-center">
                    <div class="content">

                        <h1 class="uppercase ultrabold center-text fa-4x bottom-25 color-white text-shadow-large">
                            carmurs</h1>
                        <p class="center-text bottom-50 opacity-50 font-14 color-white text-shadow-large">
                            Aplikasi .<br>It's easy to edit or create more.
                        </p>
                        <ul class="demo-colors">
                            <li><a href="#" class="democ-1 color-white bottom-10 no-border"><i
                                        class="shadow-icon-large gradient-body-1"></i>Default</a></li>
                            <li><a href="#" class="democ-5 color-white bottom-10 no-border"><i
                                        class="shadow-icon-large gradient-body-5"></i>Violet</a></li>
                            <li><a href="#" class="democ-7 color-white bottom-10 no-border"><i
                                        class="shadow-icon-large gradient-body-7"></i>Magenta</a></li>
                            <li><a href="#" class="democ-6 color-white bottom-10 no-border"><i
                                        class="shadow-icon-large gradient-body-6"></i>Red</a></li>
                            <li><a href="#" class="democ-3 color-white bottom-10 no-border"><i
                                        class="shadow-icon-large gradient-body-3"></i>Green</a></li>
                            <li><a href="#" class="democ-4 color-white bottom-10 no-border"><i
                                        class="shadow-icon-large gradient-body-4"></i>Blue</a></li>
                            <li><a href="#" class="democ-2 color-white bottom-10 no-border"><i
                                        class="shadow-icon-large gradient-body-2"></i>Orange</a></li>
                            <li><a href="#" class="democ-8 color-white bottom-10 no-border"><i
                                        class="shadow-icon-large gradient-body-8"></i>Dark</a></li>
                            <li><a href="#" class="democ-9 color-white bottom-10 no-border"><i
                                        class="shadow-icon-large gradient-body-9"></i>Yellow</a></li>
                        </ul>
                        <div class="clear"></div>
                        <a href="#"
                            class="close-menu top-40 button button-round button-s button-white uppercase ultrabold button-center shadow-icon-small">AWESOME</a>
                    </div>
                </div>
            </div>
        </div>
        <div id="menu-bell" data-menu-height="425" class="menu menu-box-top bg-white">
            <a href="#" class="bell-item">
                <img class="preload-image shadow-icon-large" src="<?php echo base_url() ?>assets/images/empty.png"
                    data-src="images/pictures_people/4s.png">
                <strong>Belum ada Notifikasi</strong><span class="bg-green2-dark">FRESH</span>

            </a>

            <div class="menu-bar">
                <div></div>
            </div>
        </div>
        <div id="menu-phone" data-menu-height="285" class="menu menu-box-bottom bg-white">
            <h1 class="center-text top-20 bottom-0 uppercase ultrabold">GIVE US A CALL</h1>
            <p class="color-black center-text font-12 opacity-50 top-0">Tap the number you want to call, we'll connect
                you.</p>
            <div class="phone-boxes top-20">
                <a href="tel:(234) 567 890" class="phone-box">
                    <i class="shadow-icon-large fa fa-user bg-blue-dark"></i>
                    <em>John Doe</em>
                    <strong>(234) 567 890</strong>
                </a>
                <a href="tel:(234) 567 890" class="phone-box">
                    <i class="shadow-icon-large fa fa-briefcase bg-green-dark"></i>
                    <em>Office</em>
                    <strong>(234) 567 890</strong>
                </a>
                <a href="tel:(234) 567 890" class="phone-box">
                    <i class="shadow-icon-large fa fa-life-ring font-24 bg-orange-dark"></i>
                    <em>Support</em>
                    <strong>(234) 567 890</strong>
                </a>
                <div class="clear"></div>
            </div>
            <div class="decoration opacity-50 bottom-10"></div>
            <p class="center-text font-10 small-line-height top-10">Monday - Friday | 09:00 AM to 10:00 PM</p>
        </div>
        <div id="menu-share" data-menu-height="375" class="menu menu-box-bottom bg-white">
            <h1 class="center-text top-20 bottom-0 uppercase ultrabold">Sharing the Love</h1>
            <p class="color-black center-text font-12 opacity-50 top-0">Share your page with the world, just tap an
                icon.</p>
            <div class="share-icons">
                <a href="#" class="shareToFacebook"><i
                        class="fab fa-facebook-f shadow-icon-large facebook-bg"></i><em>Facebook</em></a>
                <a href="#" class="shareToGooglePlus"><i
                        class="fab fa-google-plus-g shadow-icon-large google-bg"></i><em>Google +</em></a>
                <a href="#" class="shareToTwitter"><i
                        class="fab fa-twitter shadow-icon-large twitter-bg"></i><em>Twitter</em></a>
                <a href="#" class="shareToPinterest"><i
                        class="fab fa-pinterest-p shadow-icon-large pinterest-bg"></i><em>Pinterest</em></a>
                <a href="#" class="shareToWhatsApp"><i
                        class="fab fa-whatsapp shadow-icon-large whatsapp-bg"></i><em>WhatsApp</em></a>
                <a href="#" class="shareToMail"><i
                        class="far fa-envelope shadow-icon-large mail-bg"></i><em>Email</em></a>
            </div>
            <p class="center-text font-10 small-line-height top-10">Copyright <span class="copyright-year"></span>
                Enabled. All Rights Reserved.</p>
        </div>
        <!-- content -->
        <div class="page-content header-clear-large">
            <!-- card list -->
            <div data-article-card="article-1" class="article-card bg-white shadow-large">
                <a data-deploy-card="article-1" href="#" class="article-header">
                    <span class="article-overlay"></span>
                    <span class="article-image preload-image"
                        data-src="<?php base_url()?>assets/images/rs/1.jpg"></span>
                    <span class="article-category bg-red-dark bg-border-light uppercase">Rumah Sakit Ciputra Citra
                        Garden City</span>
                    <span class="article-author color-gray-light"><i class="fa fa-calendar opacity-50"></i>SENIN -
                        MINGGU</span>
                    <span class="article-time color-gray-light">5 tahun beroperasi<i
                            class="fa fa-clock opacity-50"></i></span>
                </a>
                <div class="article-content">
                    <h1 class="article-title font-14 bold bottom-15 top-15">Alamat
                        Jl. Satu Maret - Kalideres, Jakarta Barat, Jakarta, Indonesia 11820
                    </h1>
                    <a href="https://www.google.com/maps?saddr=Current+Location&daddr=-6.12682560000000000000,106.70436460000000000000&hl=en"
                        class="button button-s button-round shadow-icon-large button-full bottom-15 button-blue ultrabold uppercase">
                        <i class="fa fa-location-arrow"></i>
                        Jarak Ke Lokasi</a>

                </div>
                <div class="article-content-hidden">
                    <div class="social-icons top-30 bottom-15">
                        <a href="#" class="icon icon-xs icon-round shadow-icon-large facebook-bg"><i
                                class="fab fa-facebook-f"></i></a>
                        <a href="#" class="icon icon-xs icon-round shadow-icon-large twitter-bg"><i
                                class="fab fa-twitter"></i></a>
                        <a href="#" class="icon icon-xs icon-round shadow-icon-large google-bg"><i
                                class="fab fa-google-plus"></i></a>
                        <a href="#" class="icon icon-xs icon-round shadow-icon-large pinterest-bg"><i
                                class="fab fa-pinterest-p"></i></a>
                    </div>
                    <div class="decoration"></div>
                    <h5 class="bold">Deskripsi</h5>
                    <p class="bottom-30">
                        TCiputra Hospital Jakarta adalah rumah sakit yang berada di Citra 5, perumahan CitraGarden City
                        Jakarta. Ciputra Hospital di CitraGarden City merupakan rumah sakit kedua, setelah yang pertama
                        dibangun di CitraRaya Cikupa Tangerang. Ciputra Hospital dibangun oleh Grup Ciputra di bawah PT.
                        Ciputra Development Tbk, yang dibangun untuk memberikan layanan kesehatan terpadu bagi warga
                        yang tinggal di CitraGarden City maupun masyarakat yang ada di sekitarnya.
                        Ciputra Hospital di Cengkareng ini dirancang dengan konsep modern, bersih dan hijau (modern,
                        clean & green) serta didukung dengan fasilitas yang lengkap dan nyaman. Ciputra Hospital enjawab
                        kebutuhan anda di bidang pelayanan kesehatan yang terpadu mulai dari layanan preventif
                        (mempromosikan dan mengedukasi gaya hidup sehat dan pencegahan terhadap penyakit), layanan
                        kuratif (pengobatan terhadap berbagai penyakit) dan layanan rehabilitatif (pemulihan pasca
                        pengobatan).
                        Ini sejalan dengan motto “ Care For Your Health & Happiness”.
                    </p>
                  

                    <div class="clear"></div>
                    <a href="#"
                        class="close-article button button-s button-round shadow-icon-large button-full bottom-15 button-blue ultrabold uppercase">Back
                        to Articles</a>
                </div>
            </div>

            <!-- list 2 -->
            <div data-article-card="article-1" class="article-card bg-white shadow-large">
                <a data-deploy-card="article-1" href="#" class="article-header">
                    <span class="article-overlay"></span>
                    <span class="article-image preload-image"
                        data-src="<?php base_url()?>assets/images/rs/2.jpg"></span>
                    <span class="article-category bg-red-dark bg-border-light uppercase">Rumah Sakit Mata Aini Prof. Dr.
                        Isak Salim</span>
                    <span class="article-author color-gray-light"><i class="fa fa-calendar opacity-50"></i>SENIN -
                        MINGGU</span>
                    <span class="article-time color-gray-light">5 tahun beroperasi<i
                            class="fa fa-clock opacity-50"></i></span>
                </a>
                <div class="article-content">
                    <h1 class="article-title font-14 bold bottom-15 top-15">Alamat
                        Jl. Rumah Sakit Mata AINI, HR. Rasuna Said - Setiabudi, Jakarta Selatan, Jakarta, Indonesia
                        12920
                    </h1>
                    <a href="https://www.google.com/maps?saddr=Current+Location&daddr=-6.21427815272572200000,106.82849521957405000000&hl=en"
                        class="button button-s button-round shadow-icon-large button-full bottom-15 button-blue ultrabold uppercase">
                        <i class="fa fa-location-arrow"></i>
                        Jarak Ke Lokasi</a>

                </div>
                <div class="article-content-hidden">
                    <div class="social-icons top-30 bottom-15">
                        <a href="#" class="icon icon-xs icon-round shadow-icon-large facebook-bg"><i
                                class="fab fa-facebook-f"></i></a>
                        <a href="#" class="icon icon-xs icon-round shadow-icon-large twitter-bg"><i
                                class="fab fa-twitter"></i></a>
                        <a href="#" class="icon icon-xs icon-round shadow-icon-large google-bg"><i
                                class="fab fa-google-plus"></i></a>
                        <a href="#" class="icon icon-xs icon-round shadow-icon-large pinterest-bg"><i
                                class="fab fa-pinterest-p"></i></a>
                    </div>
                    <div class="decoration"></div>
                    <h5 class="bold">Deskripsi</h5>
                    <p class="bottom-30">
                        Rumah Sakit Mata Aini Prof. Dr. Isak Salim merupakan sebuah Rumah Sakit yang berlokasi di
                        Setiabudi, Jakarta Selatan, Jakarta, Indonesia. Saat ini, dokter-dokter yang melakukan praktek
                        di Rumah Sakit Mata Aini Prof. Dr. Isak Salim diantaranya adalah dr. Lumongga B. Simangunsong,
                        Sp.M, dr. Bondan Harmani, Sp.M, dr. Nida Farida, Sp.M, dan dokter lainnya. Adapun beberapa
                        layanan kesehatan yang dapat Anda temukan di Rumah Sakit Mata Aini Prof. Dr. Isak Salim adalah
                        Apotek, Laboratorium, Mata, Penyakit Dalam, Gawat Darurat, dan lain-lain.

                        Anda dapat mengetahui jadwal konsultasi dokter serta melakukan reservasi online di Rumah Sakit
                        Mata Aini Prof. Dr. Isak Salim melalui kami. Jadwal
                    </p>
                  

                    <div class="clear"></div>
                    <a href="#"
                        class="close-article button button-s button-round shadow-icon-large button-full bottom-15 button-blue ultrabold uppercase">Back
                        to Articles</a>
                </div>
            </div>
            <!-- list 2 -->
            <div data-article-card="article-1" class="article-card bg-white shadow-large">
                <a data-deploy-card="article-1" href="#" class="article-header">
                    <span class="article-overlay"></span>
                    <span class="article-image preload-image"
                        data-src="<?php base_url()?>assets/images/rs/3.png"></span>
                    <span class="article-category bg-red-dark bg-border-light uppercase">Rumah Sakit Cipto Mangunkusumo
                        - Kencana</span>
                    <span class="article-author color-gray-light"><i class="fa fa-calendar opacity-50"></i>SENIN -
                        MINGGU</span>
                    <span class="article-time color-gray-light">10 tahun beroperasi<i
                            class="fa fa-clock opacity-50"></i></span>
                </a>
                <div class="article-content">
                    <h1 class="article-title font-14 bold bottom-15 top-15">Alamat
                        Jl. Diponegoro No. 71 - Menteng, Jakarta Pusat, Jakarta, Indonesia 10430


                    </h1>
                    <a href="https://www.google.com/maps?saddr=Current+Location&daddr=-6.19838809920180200000,106.84575877517705000000&hl=en"
                        class="button button-s button-round shadow-icon-large button-full bottom-15 button-blue ultrabold uppercase">
                        <i class="fa fa-location-arrow"></i>
                        Jarak Ke Lokasi</a>

                </div>
                <div class="article-content-hidden">
                    <div class="social-icons top-30 bottom-15">
                        <a href="#" class="icon icon-xs icon-round shadow-icon-large facebook-bg"><i
                                class="fab fa-facebook-f"></i></a>
                        <a href="#" class="icon icon-xs icon-round shadow-icon-large twitter-bg"><i
                                class="fab fa-twitter"></i></a>
                        <a href="#" class="icon icon-xs icon-round shadow-icon-large google-bg"><i
                                class="fab fa-google-plus"></i></a>
                        <a href="#" class="icon icon-xs icon-round shadow-icon-large pinterest-bg"><i
                                class="fab fa-pinterest-p"></i></a>
                    </div>
                    <div class="decoration"></div>
                    <h5 class="bold">Deskripsi</h5>
                    <p class="bottom-30">
                        Rumah Sakit Cipto Mangunkusumo - Kencana merupakan sebuah Rumah Sakit yang berlokasi di Menteng,
                        Jakarta Pusat, Jakarta, Indonesia. Saat ini, dokter-dokter yang melakukan praktek di Rumah Sakit
                        Cipto Mangunkusumo - Kencana diantaranya adalah Dr.dr. Rahyussalim, Sp.OT(K)-Spine, dr. Erwin
                        Danil Julian, Sp.B(K)Onk, dr. Diani Kartini, Sp.B(K)Onk, dan dokter lainnya. Adapun beberapa
                        layanan kesehatan yang dapat Anda temukan di Rumah Sakit Cipto Mangunkusumo - Kencana adalah
                        Anak, Gigi, Apotek, Praktek Bidan, Kebidanan dan Kandungan, dan lain-lain. Fasilitas kesehatan
                        ini telah berdiri sejak tahun 2010.

                    </p>
                  

                    <div class="clear"></div>
                    <a href="#"
                        class="close-article button button-s button-round shadow-icon-large button-full bottom-15 button-blue ultrabold uppercase">Back
                        to Articles</a>
                </div>
            </div>
        </div>


        <a href="#" class="back-to-top-badge back-to-top-small shadow-large bg-blue-dark"><i
                class="fa fa-angle-up"></i>Back to Top</a>
    </div>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/scripts/jquery.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/scripts/plugins.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/scripts/custom.js"></script>
</body>