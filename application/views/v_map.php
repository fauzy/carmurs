<!DOCTYPE html>
<html lang="id">

<head>
    <meta charset="utf-8">
    <title>Welcome to Google Map</title>
    <?php echo $map['js'];?>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1" />
    <title>carmurs</title>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/fonts/css/fontawesome-all.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/styles/framework.css">
    <link
        href="../../../fonts.googleapis.com/css09dd.css?family=Poppins:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i|Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i"
        rel="stylesheet">
</head>

<body>
    <div id="preloader" class="preloader-light">
        <h1 class="center-text color-black ultrabold uppercase bottom-0 fa-2x">carmurs</h1>
        <div id="preload-spinner"></div>
        <p>Cara Mudah Untuk Rumah Sakit</p>
        <em>Sekolah tinggi ilmu kesehatan </em>
    </div>
    <div id="page-transitions" class="page-build bg-white">
        <div class="page-content page-content-full">
            <a href="#" class="map-but-1 cover-back-button back-button"><i
                    class="fa fa-chevron-left font-12 color-white"></i></a>
            <a href="<?php base_url()?>" class="map-but-2 cover-home-button"><i class="fa fa-home font-14 color-white"></i></a>
            <div class="map-full cover-item">
                <?php echo $map['html'];?>
                <div class="cover-content cover-content-center">
                    <h1 class="center-text uppercase bolder color-white fa-3x bottom-15">Activate Map</h1>
                    <p class="boxed-text-large center-text color-white opacity-70 bottom-30 font-12">
					Untuk mengaktifkan peta cukup ketuk tombol di bawah ini. Setelah selesai, ketuk tombol tutup peta di
                         bagian bawah.
                    </p>
                    <a href="#"
                        class="show-map button button-s button-green button-round shadow-icon-large button-center-medium uppercase ultrabold">Show
                        Map</a>
                </div>
                <a href="#"
                    class="hide-map button button-s button-center-medium button-red button-round shadow-icon-large uppercase ultrabold">Disable
                    Map</a>
                <div class="cover-overlay overlay bg-black opacity-90"></div>
            </div>
        </div>
    </div>




    <script type="text/javascript" src="<?php echo base_url() ?>assets/scripts/jquery.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/scripts/plugins.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/scripts/custom.js"></script>
</body>

</html>